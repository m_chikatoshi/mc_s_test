﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC_S_test
{
    class Program
    {
        static void Main(string[] args)
        {

            //四則演算
            //int x = 10; 
            //int y = 5;

            //Console.WriteLine(x + y);
            //Console.WriteLine(x - y);
            //Console.WriteLine(x * y);
            //Console.WriteLine(x / y);

            //変換　
            //double x = 5.5;　精度の高い数値
            //int i = (int)x;　doubleをintに変換すると小数点が失われる
            //Console.WriteLine(x);表示は5.5
            //Console.WriteLine(i);表示は.5が失われて5になる

            //文字列
            //string s = "サンプルテキスト";//sの定義
            //Console.WriteLine(s + "追加のテキスト");//+" "でテキストを連結
            //Console.WriteLine(string.Format("({0},{1})",10,20)); //{0}の所に10が入る。{1}の所に20が入る

            //配列と反復
            //int[] x = new[] { 1, 2, 3, 4, 5 };
            // int i = 0;
            //while (i < x.Length)
            //{
            //    Console.WriteLine(x[i]);
            //    ++i;
            //}

            //繰り返し(for)
            //int[] x = new[]{ 1, 2, 3, 4, 5 };
            //
            //for (int i = 0; i < x.Length; i++)//(初期値;継続条件;値の更新)
            //{
            //    Console.WriteLine(x[i]);
            //}

            //繰り返し(foreach)
            //int[] x = { 1, 2, 3, 4, 5 };
            //
            //foreach (var item in x)//配列とかのコレクションの列挙 foreach(var 変数 in 配列)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine("あなたの名前は？");
            //string name = Console.ReadLine();
            //Console.WriteLine("こんにちは{0}さん！", name);
            //const double Tax = 1.08;
            //int price = 3600;
            //double sum = price * Tax;
            //Console.WriteLine(sum);
            //Console.WriteLine(0x1111);//16進数リテラル
            //Console.WriteLine(123_456_789);//数値セパレータ
            //Console.WriteLine('\n');
            //Console.WriteLine(1.4142e11);
            //Console.WriteLine("you are \"GREAT\" teacher!!");//文字列リテラル"ｘ"で囲まれた文字列の中で""を使用するための表記
            //Console.WriteLine(@"C:\Windows\AppPatch\en-US");//\がエスケープシーケンスの為@を"文字列"の前におく
            //Console.WriteLine("2" + "3");
            //Console.WriteLine(1++);//被演算子に影響を及ぼす演算子の為エラー
            //Console.WriteLine(6 / 5);
            //Console.WriteLine(1.0 / 0);
            //Console.WriteLine(9 % 5);
            //var x = 1;
            //var y = x;
            //x += 10;
            //Console.WriteLine(x);
            //Console.WriteLine(y);

            //var builder1 = new StringBuilder("あいう");
            //var builder2 = builder1;
            //builder1.Append("えお");
            //Console.WriteLine(builder1.ToString());
            //Console.WriteLine(builder2.ToString());

            //const double EPSILON = 0.00001;
            //double x = 0.2 * 3;//EPSIONは誤差の範囲を表す。上で小数点第5位までの精度を保障。
            //double y = 0.6;//その為変数の小数点の精度を上げると誤差が大きくなるためFalseになる。（0.2→turu 0.21→False）
            //Console.WriteLine(Math.Abs(x - y) < EPSILON);

            //var data1 = new[] { "い", "ろ", "は" };
            //var data2 = new[] { "い", "ろ", "は" };
            //Console.WriteLine(data1 == data2);
            //Console.WriteLine(data1.Equals(data2));
            //Console.WriteLine(data1.SequenceEqual(data2));//配列の比較にはSewuenceEqualを使う

            //条件演算子(?:)
            //var score = 75;
            //Console.WriteLine(score >= 70 ? "合格" : "不合格");//scoreが>=70のときtureなら"合格"を表示する

            //string str = "山田";
            //string def = "権兵衛";
            //str = null;
            //Console.WriteLine(str ?? def);
            //string str = null;
            //string def = "AA";
            //Console.WriteLine(str ?? def);

            //Console.WriteLine("123".Equals(123));//fales
            //Console.WriteLine("123" == 123);
            //Console.WriteLine(new StringBuilder("X") == new StringBuilder("X"));false
            //Console.WriteLine(new[] { 1, 2, 3 }.Equals(new[] { 1, 2, 3 }));false


            //nullショートカット演算子
            //string str = null;
            //if(str != null && str.StartsWith("http://"))
            //{
            //    Console.WriteLine("「http://～」で始まります。");
            //}

            //ビット演算子

            //その他
            //Console.WriteLine(sizeof(int));
            //Console.WriteLine(sizeof(decimal));

            //条件分岐
            //if命令
            //var i = 9;
            //if (i == 10)
            //{
            //    Console.WriteLine("変数iは10です。");
            //}
            //else
            //{
            //    Console.WriteLine("変数iは10ではありません。");
            //}
            //var i = 40;
            //if (i > 50)
            //{
            //    Console.WriteLine("変数iは50より大きいです。");
            //}
            //else if (i > 30)
            //{
            //    Console.WriteLine("変数iは30より大きく、50以下です。");
            //}
            //else
            //{
            //    Console.WriteLine("変数iは30以下です。");
            //}

            //var i = 1;
            //var j = 0;
            //if (i == 1)
            //{
            //    if (j == 1)
            //    {
            //        Console.WriteLine("変数i,jは1です。");
            //    }
            //    else
            //    {
            //        Console.WriteLine("変数iは1ですが、jは1ではありません。");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("変数iは1ではありません。");
            //}

            //switch構文

            //var rank = "丁";

            //Console.WriteLine(rank);

            //switch(rank)
            //{
            //    case "甲":
            //        Console.WriteLine("大変良いです。");
            //        break;
            //
            //    case "乙":
            //        Console.WriteLine("良いです。");
            //        break;
            //
            //    case "丙":
            //        Console.WriteLine("がんばりましょう。");
            //        break;
            //
            //    default:
            //        Console.WriteLine("???");
            //        break;
            //    
            //}

            //var drink = "ブランデー";
            //Console.WriteLine(drink);
            //switch (drink)
            //{
            //    case "日本酒":
            //    case "ビール":
            //    case "ワイン":
            //        Console.WriteLine("醸造酒です。");
            //        break;
            //    case "ブランデー":
            //    case "ウィスキー":
            //        Console.WriteLine("蒸留酒です。");
            //        break;
            //    default:
            //        Console.WriteLine("???");
            //        break;
            //}

            // switch objrct型
            //object obj = -234;
            //switch(obj)
            //{
            //    //変数objがint型の場合、絶対値を求める。
            //    case int i:
            //        Console.WriteLine(Math.Abs(i));
            //        break;
            //    //変数objがstring型の場合、先頭文字を取得
            //    case string str:
            //        Console.WriteLine(str[0]);
            //        break;
            //    //それ以外の場合はエラーメッセージ
            //    default:
            //        Console.WriteLine("意図しない型です。");
            //        break;
            //
            //}
            //var i = 60;
            //if (i > 90)
            //    Console.WriteLine("優");
            //else if (i > 70)
            //    Console.WriteLine("良");
            //else if (i > 50)
            //    Console.WriteLine("可");
            //else
            //    Console.WriteLine("不可");

            //繰り返し処理
            //var i = 1;
            //while (i < 6)
            //{
            //    Console.WriteLine($"{i}番目のループです。");
            //    i++;
            //}
            //for ( var i = 1; i < 6; i++)
            //{
            //    Console.WriteLine($"{i}番目のループです。");
            //}

            //foreach
            //var data = new[] { "うめ", "さくら", "もも" };
            //foreach (var val in data)
            //{
            //    Console.WriteLine(val);
            //}
            //for (var i = 1;i < 10 ;i++)
            //{
            //    for (var j = 1; j < 10; j++)
            //    {
            //        Console.Write($"{i * j}");
            //    }
            //    Console.WriteLine();
            //}

            //break命令
            //int i;
            //int sum = 0;

            //for (i = 1;i <= 100; i++)
            //{
            //    sum += i;
            //    if (sum > 1000)
            //    {
            //        break;
            //    }
            //}

            //Console.WriteLine($"合計が1000を超えるのは、1～{i}を加算したときです。");

            //continu命令
            
            //int sum = 0;

            //for (int i = 1; i <= 100; i++)
            //{
            //    if (i % 2 != 0)
            //    {
            //    continue; 
            //    }
            //sum += i;
            //}
            
            //Console.WriteLine($"合計値は{sum}です。");
            
            
            
            //break/continue命令
            for (var i = 1; i < 10; i++)
            {
                for (var j = 1; j < 10; j++)
                {
                    var result = i * j;
                    if (result > 40)
                    {
                        break;
                    }
                    Console.Write($"{result,2}");
                }
                Console.WriteLine();
            }
        }
    }
}
